<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VacanteController as VController;
use App\Http\Controllers\CandidatoController as CAController;
use App\Http\Controllers\NotificationsController as UserNotifications;
use App\Http\Controllers\CategoriaController as CategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes(['verify' => true]);

Route::get('/', \App\Http\Controllers\InicioController::class)->name('inicio');

//Routers Protected Group
Route::group(['middleware'=>['auth', 'verified']], function(){
    //Router Vacantes
    Route::get('/vacantes', [VController::class, 'index'])->name('vacantes.index');
    Route::get('/vacantes/create', [VController::class, 'create'])->name('vacantes.create');
    Route::post('/vacantes', [VController::class, 'store'])->name('vacantes.store');
    Route::delete('/vacantes/{vacante}', [VController::class, 'destroy'])->name('vacantes.destroy');
    Route::get('/vacantes/{vacante}/edit', [VController::class, 'edit'])->name('vacantes.edit');
    Route::put('/vacantes/{vacante}',[VController::class, 'update'])->name('vacantes.update');

    //Router Upload Image
    Route::post('/vacantes/imagen', [VController::class, 'imagen'])->name('upload.imagen');
    Route::post('/vacantes/borrarImagen', [VController::class, 'borrarImagen'])->name('upload.imagenborrar');

    //Change State Vacante
    Route::post('/vacantes/{id}', [VController::class, 'updateState'])->name('vacantes.updateState');

    //Notify
    Route::get('/notificaciones', UserNotifications::class)->name('notificaciones');

});
//Show Category
Route::get('/categorias/{categoria}', [CategoryController::class, 'show'])->name('category.show');

//Search Route
Route::post('/search/search',[VController::class, 'search'])->name('vacantes.search');
Route::get('/search/search',[VController::class, 'results'])->name('vacantes.results');

//Inscription Contact
Route::post('/candidatos/store', [CAController::class, 'store'])->name('cantidatos.store');

//Show Not Protected
Route::get('/candidatos/{id}', [CAController::class, 'index'])->name('candidatos.index');
Route::get('/vacantes/{vacante}', [VController::class, 'show'])->name('vacantes.show');





