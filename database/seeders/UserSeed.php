<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'David Valenzuel Pardo',
            'email' => 'vlzdavid12@outlook.com',
            'email_verified_at'=>'2021-04-19 16:54:54',
            'url' => 'http:media.com.co',
            'password' => '123456789',
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now(),
        ]);
    }
}
