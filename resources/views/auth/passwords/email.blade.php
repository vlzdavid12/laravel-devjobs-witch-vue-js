@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        <div class="flex justify-center">
            <div class="w-96 flex flex-wrap content-center justify-center">

                <div class="w-full max-w-sm">
                    <div class="flex  flex-col break-words bg-white border border-2 shadow-md mt-20">

                <h2
                    class="bg-gray-300 text-gray-700 uppercase text-center py-3 px-6 mb-0 uppercase font-semibold">
                    {{ __('Reset Password') }}</h2>

                    @if (session('status'))
                        <div class="text-green-400 py-2 block text-center" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="p-5">
                    <form method="POST" action="{{ route('password.email') }}" novalidate>
                        @csrf

                        <label for="email" class="text-gray-700 block my-3">{{ __('E-Mail Address') }}</label>

                            <input id="email" type="email"
                                   class="p-3 bg-gray-100 rounded w-full @error('email') border-red-400 border @enderror"
                                   name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                                       {{ $message }}
                                    </div>
                            @enderror


                        <button type="submit"
                                class="bg-gray-700 py-3 text-center text-white w-full rounded hover:bg-gray-800 focus:outline-none mt-4">
                            {{ __('Send Password Reset Link') }}
                        </button>

                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
