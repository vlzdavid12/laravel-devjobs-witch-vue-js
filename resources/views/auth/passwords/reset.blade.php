@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        <div class="flex flex-wrap justify-center">
            <div class="w-full max-w-lg text-gray-600">
                <div class="bg-white p-10 my-10">
                <div class="text-center text-3xl text-blue-500 mb-10 uppercase">{{ __('Reset Password') }}</div>

                    <form method="POST" action="{{ route('password.update') }}" novalidate >
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">


                            <label for="email" class="text-gray-700 block my-3">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email"
                                       class="p-3 bg-gray-100 rounded w-full @error('email') border-red-400 border @enderror"
                                       name="email"
                                       value="{{ $email ?? old('email') }}"
                                       autocomplete="email" autofocus>

                                @error('email')
                                    <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                                       {{ $message }}
                                    </div>
                                @enderror
                            </div>


                            <label for="password" class="text-gray-700 block my-3">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password"
                                       type="password"
                                       class="p-3 bg-gray-100 rounded w-full @error('password') border-red-400 border @enderror"
                                       name="password"
                                       autocomplete="new-password">

                                @error('password')
                                    <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                                     {{ $message }}
                                @enderror
                            </div>


                        <div class="form-group row">
                            <label for="password-confirm" class="text-gray-700 block my-3">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="p-3 bg-gray-100 rounded w-full" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="bg-gray-700 py-3 text-center text-white w-full rounded hover:bg-gray-800 focus:outline-none mt-4">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>

@endsection
