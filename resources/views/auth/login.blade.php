@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        <div class="flex flex-wrap justify-center">
            <div class="w-full max-w-sm">
                <div class="flex  flex-col break-words bg-white border border-2 shadow-md mt-20">
                    <div class="bg-gray-300 text-gray-700 uppercase text-center py-3 px-6 mb-0 uppercase font-semibold">
                        {{ __('Login') }}
                    </div>

                    <div class="p-5">
                        <form method="POST" action="{{ route('login') }}" novalidate >
                            @csrf

                            <div class="form-group row">
                                <label for="email"
                                       class="text-gray-700 block my-3">{{ __('E-Mail Address') }}</label>

                                <input id="email" type="email"
                                       class="p-3 bg-gray-100 rounded w-full @error('email') border-red-400 border @enderror"
                                       name="email" value="{{ old('email') }}"
                                       autocomplete="email"
                                       autofocus>

                                @error('email')
                                <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                                   {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                       class="text-gray-700 block my-3">{{ __('Password') }}</label>
                                <input id="password" type="password"
                                       class="p-3 bg-gray-100 rounded w-full @error('password') border-red-400 border @enderror"
                                       name="password"
                                       autocomplete="current-password">

                                @error('password')
                                <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                                      {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="flex">
                                <input class="w-5" type="checkbox" name="remember"
                                       id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="my-2 mx-2 text-gray-700" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>

                            <div class="block m-3">
                                <button type="submit"
                                        class="bg-gray-700 py-3 text-center text-white w-full rounded hover:bg-gray-800 focus:outline-none"
                                >
                                    {{ __('Login') }}
                                </button>
                            </div>
                            @if (Route::has('password.request'))
                                <div class="block text-center mt-5">
                                    <a class="text-center text-blue-700 hover:text-blue-900"
                                       href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                </div>
                            @endif

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
