@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        <div class="flex justify-center">
            <div class="w-96 flex flex-wrap content-center justify-center">
                <h2 class="text-blue-500 text-2xl uppercase">{{__('¿Eres Reclutador?')}}</h2>
                <p class="text-lg text-gray-700 text-center mt-4">{{__('Crea una cuenta gratis y comienza publicar hasta 10 vancantes.')}}</p>
            </div>
            <div class="w-full max-w-sm">
                <div class="flex  flex-col break-words bg-white border border-2 shadow-md mt-20">
                    <div class="bg-gray-300 text-gray-700 uppercase text-center py-3 px-6 mb-0 uppercase font-semibold">
                        {{ __('Register') }}
                    </div>

                    <div class="p-5">
                        <form method="POST" action="{{ route('register') }}" novalidate>
                            @csrf

                            <label for="name" class="text-gray-700 block my-3">{{ __('Name') }}</label>


                            <input id="name" type="text"
                                   class="p-3 bg-gray-100 rounded w-full @error('name') border-red-400 border @enderror"
                                   name="name"
                                   value="{{ old('name') }}"
                                   autocomplete="name"
                                   autofocus>

                            @error('name')
                            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                                       {{ $message }}
                                    </div>
                            @enderror


                            <label for="email" class="text-gray-700 block my-3">{{ __('E-Mail Address') }}</label>

                            <input id="email" type="email"
                                   class="p-3 bg-gray-100 rounded w-full @error('email') border-red-400 border @enderror"
                                   name="email"
                                   value="{{ old('email') }}"
                                   autocomplete="email">

                            @error('email')
                            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                                      {{ $message }}
                                    </div>
                            @enderror


                            <label for="url" class="text-gray-700 block my-3">{{ __('Pagina web') }}</label>


                            <input id="url"
                                   type="text"
                                   class="p-3 bg-gray-100 rounded w-full @error('url') border-red-400 border @enderror"
                                   name="url"
                                   value="{{ old('url') }}"
                                   autocomplete="url"
                                   autofocus>

                            @error('url')
                            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                                       {{ $message }}
                                    </div>
                            @enderror


                            <label for="password" class="text-gray-700 block my-3">{{ __('Password') }}</label>


                            <input id="password" type="password"
                                   class="p-3 bg-gray-100 rounded w-full @error('password') border-red-400 border @enderror"
                                   name="password"
                                   autocomplete="new-password">

                            @error('password')
                            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                                {{ $message }}
                                    </div>
                            @enderror

                            <label for="password-confirm"
                                   class="text-gray-700 block my-3">{{ __('Confirm Password') }}</label>

                            <input id="password-confirm" type="password" class="p-3 bg-gray-100 rounded w-full"
                                   name="password_confirmation"
                                   autocomplete="new-password">

                            <div class="block m-3">
                                <button type="submit"
                                        class="bg-gray-700 py-3 text-center text-white w-full rounded hover:bg-gray-800 focus:outline-none">
                                    {{ __('Register') }}
                                </button>
                            </div>

                        </form>

                    </div>
                </div>
@endsection
