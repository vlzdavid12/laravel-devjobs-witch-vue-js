@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        <div class="flex flex-wrap justify-center">
            <div class="w-full max-w-lg text-gray-600">
                <div class="bg-white p-10">
                <h2 class="text-center m-5 text-3xl text-blue-500">{{ __('Verify Your Email Address') }}</h2>

                <div class="contents">
                    @if (session('resent'))
                        <div class="text-green-500 py-2 block text-center" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    <p class="my-2 text-center leading-6">{{ __('Before proceeding, please check your email for a verification link.') }}</p>
                     <div class="mt-6">
                    <p class="text-center my-2">{{ __('If you did not receive the email') }}.</p>
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit"
                                class="text-center w-full text-white bg-gray-700 p-3 rounded ">{{ __('Pulsa para verificar tu cuenta') }}</button>

                    </form>
                     </div>
                </div>
                </div>
            </div>
        </div>
    </div>

@endsection
