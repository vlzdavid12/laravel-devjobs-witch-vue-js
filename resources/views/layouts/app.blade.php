<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    @yield('styles')
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">



</head>
<body class="bg-gray-200 min-h-screen leading-none">
@if(session('state'))
    <div class="bg-brown-200 text-brown-600 text-center p-4 font-bold uppercase">
        {{session('state')}}
    </div>
@endif
<div id="app">
    <nav class="bg-gray-800 shadow-md p-2">
        <div class="container mx-auto md: px-0">
            <div class="flex items-center justify-around">
                <a class="text-2xl text-white" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="flex-1 text-right items-stretch">
                    <!-- Right Side Of Navbar -->
                    @guest
                        @if (Route::has('login'))
                            <a class="text-white no-underline hover:underline m-1 hover:text-gray-300 p-3"
                               href="{{ route('login') }}">{{ __('Login') }}</a>
                        @endif

                        @if (Route::has('register'))
                            <a class="text-white no-underline hover:underline m-1 hover:text-gray-300 p-3"
                               href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    @else
                        @if (request()->route()->getName() == 'inicio')
                            <a class="text-gray-500 text-sm pr-4" href="{{route('vacantes.index')}}">
                                {{__('Administrador')}}
                            </a>
                        @endif
                        <span class="text-gray-500 text-sm pr-4">{{ Auth::user()->name }}</span>
                        <a href="{{route('notificaciones')}}" class="rounded_count" >
                            <span>{{Auth::user()->unreadNotifications->count()}}</span>
                        </a>
                        <a class="no-underline hover:underline text-gray-300 text-sm p-3"
                           href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                </div>
                @endguest
            </div>
        </div>
    </nav>
    <div class="bg-gray-700" >
            <nav class="container mx-auto flex space-x-1">
                @yield('navegacion')
            </nav>
    </div>

    <main class="py-4">
        @yield('content')
    </main>
</div>
@yield('scripts')
</body>
</html>
