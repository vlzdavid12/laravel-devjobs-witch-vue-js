@extends('layouts.app')

@section('navegacion')
    @include('ui.nav')
@endsection

@section('content')
    <h2 class="text-gray-700 text-center text-2xl uppercase mt-5">Candidatos</h2>

    @if(count($candidatos) > 0)

        <ul>
            @foreach($candidatos as $candidato)
                <li class="shadow w-1/3 m-auto my-5 rounded">
                    <div class="p-5">
                        <p class="my-2">
                            <span class="text-blue-600">Nombre:</span>
                            {{ $candidato->nombre }}
                        </p>
                        <p class="my-2">
                            <span class="text-blue-600">Email:</span>
                            {{ $candidato->email }}
                        </p>
                        <p class="my-2">
                            <span class="text-blue-600">Curriculum</span>
                            <a href="/storage/cv/{{ $candidato->cv }}" target="_blank"/> Hoja de Vida </a>
                        </p>
                        <p class="my-2">
                            <span class="text-blue-600"> Creado: </span>
                            {{ $candidato->created_at }}
                        </p>
                        <p class="my-2">
                            <span class="text-blue-600"> Actualizado: </span>
                            {{ $candidato->updated_at }}
                        </p>
                    </div>
                </li>
            @endforeach
        </ul>
    @else
        <div class="flex justify-center items-center">
            <h3 class="text-gray-700 mt-10">No hay resultados de candidatos</h3>
        </div>
    @endif


@endsection
