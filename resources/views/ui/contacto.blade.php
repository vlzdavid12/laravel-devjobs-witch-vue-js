<aside class="w-full m-auto  bg-gray-300 py-3">
    <h2 class="text-center text-gray-600 text-2xl p-3 m-3 bg-gray-400">Contacta al Reclutador</h2>
    <form enctype="multipart/form-data" action="{{route('cantidatos.store')}}" method="POST">
        @csrf
        <div class="px-4 py-1 m-auto container">
            <laberl class="my-2 text-gray-500 block " for="nombre">Nombre</laberl>
            <input type="text"
                   id="nombre"
                   name="nombre"
                   placeholder="Ingresa el nombre completo"
                   value="{{old('nombre')}}"
                   class="w-full p-4 block form-input rounded @error('nombre') border border-red-500 @enderror"
            />
            @error('nombre')
            <div class="bg-red-100 border-l-4 border-red-500 text-red-500 p-4 w-full mt-5" role="alert">
                <strong class="mb-2 block">¡Error!</strong>
                <p>{{$message}}</p>
            </div>
            @enderror
        </div>
        <div class="px-4 py-1  m-auto container">
            <laberl class="my-2 text-gray-500 block" for="nombre">Email</laberl>
            <input type="text"
                   id="email"
                   name="email"
                   placeholder="Ingresa el correo electrónico"
                   value="{{old('email')}}"
                   class="w-full p-4 block form-input rounded @error('email') border border-red-500 @enderror"
            />
            @error('email')
            <div class="bg-red-100 border-l-4 border-red-500 text-red-500 p-4 w-full mt-5" role="alert">
                <strong class="mb-2 block">¡Error!</strong>
                <p>{{$message}}</p>
            </div>
            @enderror
        </div>
        <div class="px-4 py-1  m-auto container">
            <laberl class="my-2 text-gray-500 block" for="cv">Archivo</laberl>
            <input type="file"
                   id="cv"
                   name="cv"
                   accept="application/pdf"
                   class="w-full p-4 block form-input rounded @error('cv') border border-red-500 @enderror"
            />
            @error('cv')
            <div class="bg-red-100 border-l-4 border-red-500 text-red-500 p-4 w-full mt-5" role="alert">
                <strong class="mb-2 block">¡Error!</strong>
                <p>{{$message}}</p>
            </div>
            @enderror
        </div>
        <input type="hidden" name="vacante_id" id="vacante_id" value="{{$vacante->id}}" />

        <input type="submit"
               class="bg-teal-600 hover:bg-teal-700 w-11/12 m-auto block text-white p-3 cursor-pointer mt-3 uppercase rounded"
               value="Contactar" />
    </form>
</aside>
