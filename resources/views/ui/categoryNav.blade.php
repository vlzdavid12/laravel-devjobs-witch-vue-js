<ul class="flex flex-wrap">
    <li class="p-2 bg-blue-500 text-white p-3 uppercase">{{__('Categorias')}}</li>
@foreach($categorias as $categoria)
    <li class="p-3">
        <a href="{{route('category.show', ['categoria' => $categoria->id])}}"
           target="__blank" class="text-gray-200 hover:text-blue-300 pb-2" >{{$categoria->nombre}}</a>
    </li>
    @endforeach
</ul>
