<h3 class="my-10 text-1xl text-gray-700">Buscar Vacantes</h3>
<form method="POST" action="{{route('vacantes.search')}}" class="relative">
    @csrf
    {{-- Nivel de Studio --}}
    <div class="mb-5">
        <label for="categoria"
               class="block text-gray-700 text-sm mb-2 text-gray-500"> {{__('Categoria')}}</label>
        <select
            id="categoria"
            name="categoria"
            class="block appearance-none w-full border border-gray-200 text-gray-700 rounded leading-tight focus:outline-none focus:border-gray-500 p-3 bg-gray-100
                    @error('categoria') border-red-400 border @enderror"
        >
            <option disabled selected>Seleccione Nivel</option>
            @foreach($categorias as $categoria)
                <option
                    {{old('categoria') == $categoria->id? 'selected' : ''}}
                    value="{{$categoria->id}}">{{$categoria->nombre}}</option>
            @endforeach
        </select>
        @error('categoria')
        <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
            <strong class="block">!Error¡</strong>
            {{ $message }}
        </div>
        @enderror
    </div>
    {{-- Ubicacion en Donde --}}
    <div class="mb-5">
        <label for="ubicacion"
               class="block text-gray-700 text-sm mb-2 text-gray-500"> {{__('Ubicación?')}}</label>
        <select
            id="ubicacion"
            name="ubicacion"
            class="block appearance-none w-full border border-gray-200 text-gray-700 rounded leading-tight focus:outline-none focus:border-gray-500 p-3 bg-gray-100
                        @error('ubicacion') border-red-400 border @enderror"
        >
            <option disabled selected>Seleccione Ubicación</option>
            @foreach($ubicaciones as $ubicacion)
                <option
                    {{old('ubicacion') == $ubicacion->id? 'selected' : ''}}
                    value="{{$ubicacion->id}}">{{$ubicacion->nombre}}</option>
            @endforeach
        </select>
        @error('ubicacion')
        <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
            <strong class="block">!Error¡</strong>
            {{ $message }}
        </div>
        @enderror
    </div>
    <button
        type="submit"
        class="bg-teal-500 w-full hover:bg-teal-600 text-gray-100 font-bold p-3 focus:outline-none rounded uppercase"
    >{{__('Buscar')}}</button>

</form>
