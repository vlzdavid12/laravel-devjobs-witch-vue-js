@extends('layouts.app')
@section('navegacion')
    @include('ui.nav')
@endsection

@section('content')
    <h1 class="text-3xl text-center mt-20 text-gray-700 uppercase">Notificaciones</h1>

    @if(count($notificaciones) > 0)
        <ul class="max-w-md mx-auto mt-10 ">
            @foreach($notificaciones as $notificacion)
                @php
                    $data = $notificacion->data;
                @endphp
                <li class="mb-4 p-3 text-gray-700 rounded block bg-gray-300">
                    <div class="flex pt-2">
                        <div class="flex-auto self-center">
                            <span class="p-2  ">{{__('Tienes un nuevo candidato como: ')}}</span>
                        </div>
                        <div>
                            <span class="bg-blue-600 text-white p-2 rounded">{{$data["vacante"]}}</span>
                        </div>
                    </div>
                    <div class="flex mt-2">
                        <div class="flex-auto flex-auto  self-center text-xs pl-3">
                            <p>Solicitud: {{$notificacion->created_at->diffForHumans()}}</p>
                        </div>
                        <div class="flex-auto">
                            <a target="_self"
                               class="block w-full text-blue-500 p-3 text-right underline pr-5 text-xs"
                               href="{{ route('candidatos.index', ['id'=> $data['id_vacante']]) }}">
                                {{__('Ver Candidatos')}}
                            </a>
                        </div>
                    </div>

                </li>
            @endforeach
        </ul>

    @else
        <h3 class="text-center text-gray-700 mt-5">{{__('No hay vacantes')}}</h3>
    @endif


@endsection
