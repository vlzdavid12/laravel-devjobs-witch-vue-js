@extends ('layouts.app')

@section('navegacion')
    @include('ui.nav')
@endsection

@section('content')
    <h1 class="text-center text-4xl mt-10 text-gray-700">Administrar Vacantes</h1>
    <div class="m-auto max-w-6xl">
        <div class="flex flex-col mt-10">
            <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            @if(count($vacantes) > 0)
                <div
                    class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                    <table class="min-w-full">
                        <thead class="bg-gray-100 ">
                        <tr>
                            <th class="px-6 py-3 border-b border-gray-200  text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                                {{__('Titulo Vacante')}}
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200  text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                                {{__('Estado')}}
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200  text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                                {{__('Candidatos')}}
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200  text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                                {{__('Acciones')}}
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white">
                        @foreach ($vacantes as $vacante)
                            <tr>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="flex items-center">

                                        <div class="ml-4">
                                            <div class="text-sm leading-5 font-medium text-gray-900">
                                                {{$vacante->titulo}}
                                            </div>
                                            <div class="text-sm leading-5 text-gray-500">
                                                {{__('Categoria: ').__($vacante->categoria->nombre)}}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <state-vacante estado="{{$vacante->active}}" vacante-id="{{$vacante->id}}" ></state-vacante>
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                    <a
                                        href="{{route('candidatos.index', ['id' => $vacante->id])}}"
                                        class="text-gray-500 hover:text-gray-600"
                                    >{{__('Candidatos: ')}} {{$vacante->candidatos->count()}}</a>
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium">
                                    <delete-vacante :vacante-id="{{$vacante->id}}" ></delete-vacante>
                                    <a href="{{route('vacantes.edit', ['vacante' => $vacante->id])}}"
                                       class="text-green-600 hover:text-teal-900 mr-5"
                                    >{{__('Editar')}}</a>
                                    <a href="{{route('vacantes.show', ['vacante' => $vacante->id])}}" class="text-blue-600 hover:text-blue-900">{{__('Ver')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="mt-3">
                    {{$vacantes->links()}}
                </div>
                @else
                <p class="text-center text-blue-500 text-xl">{{__('No tienes vacantes aún')}}</p>
                @endif
            </div>
        </div>
    </div>
@endsection
<script>
    import EliminarVacante from "../../js/components/eliminarVacante";
    export default {
        components: {EliminarVacante}
    }
</script>
