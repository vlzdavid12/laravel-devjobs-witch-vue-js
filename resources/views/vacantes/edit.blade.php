@extends ('layouts.app')

{{--Section Yield Styles--}}
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/dropzone.min.css"
          integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A=="
          crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/css/medium-editor.css"
          integrity="sha512-iWJx03fFJWrXXXI6ctpoVaLkB6a4yf9EHNURLEEsLxGyup43eF6LrD3FSPdt1FKnGSE8Zp7JZYEDbATHf1Yx8Q=="
          crossorigin="anonymous"/>
@endsection
{{--End Section Yield Styles--}}

@section('navegacion')
    @include('ui.nav')
@endsection
@section('content')

    <h2 class="text-center text-gray-700 p-5 text-3xl uppercase">{{__('Editar Contenido ')}}{{$vacante->titulo}}</h2>

    <form method="POST" action="{{ route('vacantes.update', ['vacante'=>$vacante->id]) }}" class="max-w-lg  mx-auto  my-10" novalidate>
        @csrf
        @method('PUT')
        <div class="mb-5">
            <label for="titulo"
                   class="block text-gray-700 border border-gray-200  text-sm mb-2 text-gray-500"> {{__('Titulo Vacante')}}</label>
            <input type="text"
                   name="titulo"
                   value="{{$vacante->titulo}}"
                   class="appearance-none p-3 bg-white rounded  w-full leading-tight  focus:border-gray-500
                          @error('titulo') border-red-400 border @enderror"/>

            @error('titulo')
            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                <strong class="block">!Error¡</strong>
                {{ $message }}
            </div>
            @enderror
        </div>
        {{-- Nivel de Studio --}}
        <div class="mb-5">
            <label for="category"
                   class="block text-gray-700 text-sm mb-2 text-gray-500"> {{__('Eres?')}}</label>
            <select
                id="categoria"
                name="categoria"
                class="block appearance-none w-full border border-gray-200 text-gray-700 rounded leading-tight focus:outline-none focus:border-gray-500 p-3 bg-gray-100
                    @error('categoria') border-red-400 border @enderror"
            >
                <option disabled selected>Seleccione Nivel</option>
                @foreach($categorias as $categoria)
                    <option
                        {{$vacante->categoria->id == $categoria->id? 'selected' : ''}}
                        value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                @endforeach
            </select>
            @error('categoria')
            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                <strong class="block">!Error¡</strong>
                {{ $message }}
            </div>
            @enderror
        </div>
        {{-- Grado de Experiencia --}}
        <div class="mb-5">
            <label for="experience"
                   class="block text-gray-700 text-sm mb-2 text-gray-500"> {{__('Experiencia')}}</label>
            <select
                id="experiencia"
                name="experiencia"
                class="block appearance-none w-full border border-gray-200 text-gray-700 rounded leading-tight focus:outline-none focus:border-gray-500 p-3 bg-gray-100
                        @error('experiencia') border-red-400 border @enderror"
            >
                <option disabled selected>Seleccione Experiencia</option>
                @foreach($experiencias as $experiencia)
                    <option
                        {{$vacante->experiencia->id == $experiencia->id? 'selected' : ''}}
                        value="{{$experiencia->id}}">{{$experiencia->nombre}}</option>
                @endforeach
            </select>
            @error('experiencia')
            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                <strong class="block">!Error¡</strong>
                {{ $message }}
            </div>
            @enderror
        </div>
        {{-- Ubicacion en Donde --}}
        <div class="mb-5">
            <label for="ubicacion"
                   class="block text-gray-700 text-sm mb-2 text-gray-500"> {{__('Donde Eres?')}}</label>
            <select
                id="ubicacion"
                name="ubicacion"
                class="block appearance-none w-full border border-gray-200 text-gray-700 rounded leading-tight focus:outline-none focus:border-gray-500 p-3 bg-gray-100
                        @error('ubicacion') border-red-400 border @enderror"
            >
                <option disabled selected>Seleccione Ubicación</option>
                @foreach($ubicaciones as $ubicacion)
                    <option
                        {{$vacante->ubicacion->id == $ubicacion->id? 'selected' : ''}}
                        value="{{$ubicacion->id}}">{{$ubicacion->nombre}}</option>
                @endforeach
            </select>
            @error('ubicacion')
            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                <strong class="block">!Error¡</strong>
                {{ $message }}
            </div>
            @enderror
        </div>
        {{-- Salarios --}}
        <div class="mb-5">
            <label for="salario"
                   class="block text-gray-700 text-sm mb-2 text-gray-500"> {{__('Salario')}}</label>
            <select
                id="salario"
                name="salario"
                class="block appearance-none w-full border border-gray-200 text-gray-700 rounded leading-tight focus:outline-none focus:border-gray-500 p-3 bg-gray-100
                    @error('salario') border-red-400 border @enderror"
            >
                <option disabled selected>Seleccione Ubicación</option>
                @foreach($salarios as $salario)
                    <option
                        {{$vacante->salario->id == $salario->id? 'selected' : ''}}
                        value="{{$salario->id}}">{{$salario->nombre}}</option>

                @endforeach
            </select>
            @error('salario')
            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                <strong class="block">!Error¡</strong>
                {{ $message }}
            </div>
            @enderror
        </div>
        {{-- Description --}}
        <div class="mb-5">
            <label for="descripcion"
                   class="block text-gray-700 text-sm mb-2 text-gray-500"> {{__('Descripción del puesto')}}</label>
            <div class="editable p-5"></div>
            <input type="hidden" name="descripcion" id="descripcion" value="{{$vacante->descripcion}}"/>
            @error('descripcion')
            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                <strong class="block">!Error¡</strong>
                {{ $message }}
            </div>
            @enderror
        </div>

        {{-- Image Puesto DropZone --}}
        <div class="mb-5">
            <label for="imagen"
                   class="block text-gray-700 text-sm mb-2 text-gray-500"> {{__('Image Preview')}}</label>

            <div id="dropzoneDevJobs" class="dropzone rounded bg-gray-100">
            </div>
            <input type="hidden" name="imagen" id="imagen" value="{{$vacante->imagen}}"/>
            <div id="error-upload"></div>
            @error('imagen')
            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                <strong class="block">!Error¡</strong>
                {{ $message }}
            </div>
            @enderror
        </div>
        {{-- End Image Puesto DropZone --}}

        {{-- Hability Knowledge --}}
        <div class="mb-5">
            <label for="habilidades"
                   class="block text-gray-700 text-sm mb-2 text-gray-500 text-center"> {{__('Habilidades y Conocimientos')}}</label>
            @php
                $skills = ['HTML5', 'CSS3', 'CSSGrid', 'Flexbox', 'JavaScript', 'jQuery', 'Node', 'Angular', 'VueJS', 'ReactJS', 'React Hooks', 'Redux', 'Apollo', 'GraphQL', 'TypeScript', 'PHP', 'Laravel', 'Symfony', 'Python', 'Django', 'ORM', 'Sequelize', 'Mongoose', 'SQL', 'MVC', 'SASS', 'WordPress', 'Express', 'Deno', 'React Native', 'Flutter', 'MobX', 'C#', 'Ruby on Rails']
            @endphp
            <list-skill :skills="{{json_encode($skills)}}"
                        :oldskills="{{json_encode($vacante->habilidades)}}"></list-skill>
            <input type="hidden" value="" name="habilidades" id="habilidades"/>
            @error('habilidades')
            <div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2" role="alert">
                <strong class="block">!Error¡</strong>
                {{ $message }}
            </div>
            @enderror
        </div>
        {{-- End Hability Knowledge --}}


        <button
            type="submit"
            class="cursor-pointer bg-blue-400 block  py-3 text-center text-white w-full rounded hover:bg-blue-500 focus:outline-none"
        >
            {{__('Actualizar Vacante')}}
        </button>
    </form>

@endsection
{{--Section Yield Script--}}
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js"
            integrity="sha512-VQQXLthlZQO00P+uEu4mJ4G4OAgqTtKG1hri56kQY1DtdLeIqhKUp9W/lllDDu3uN3SnUNawpW7lBda8+dSi7w=="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/js/medium-editor.min.js"
            integrity="sha512-5D/0tAVbq1D3ZAzbxOnvpLt7Jl/n8m/YGASscHTNYsBvTcJnrYNiDIJm6We0RPJCpFJWowOPNz9ZJx7Ei+yFiA=="
            crossorigin="anonymous"></script>
    <script>
        Dropzone.autoDiscover = false;
        document.addEventListener('DOMContentLoaded', () => {
            //Medium Editor
            const editor = new MediumEditor('.editable', {
                placeholder: {
                    /* This example includes the default options for placeholder,
                       if nothing is passed this is what it used */
                    text: 'Ingresa la descripción',
                    hideOnClick: true
                },
                toolbar: {
                    buttons: ['bold', 'italic', 'underline', 'quote', 'anchor', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'orderedlist'],
                    static: true,
                    sticky: true
                }
            });

            editor.setContent(document.querySelector('#descripcion').value);

            editor.subscribe('editableInput', function (events) {
                const conten = editor.getContent();
                document.querySelector('#descripcion').value = conten;
            })


            //Dropzone
            const dropzoneDevJobs = new Dropzone('#dropzoneDevJobs', {
                url: "/vacantes/imagen",
                dictDefaultMessage: 'Sube aqui tu archivo.',
                acceptedFiles: ".png, .jpg, .jpeg, .bmp",
                addRemoveLinks: true,
                dictRemoveFile: 'Borrar Archivo',
                maxFiles: 1,
                headers: {
                    'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content,
                },
                init: function () {
                    let inputImage = document.querySelector('#imagen').value.trim()
                    if (inputImage) {
                        let imagePublic = {}
                        imagePublic.size = 1234
                        imagePublic.name = inputImage
                        this.options.addedfile.call(this, imagePublic);
                        this.options.thumbnail.call(this, imagePublic, `/storage/vacantes/${imagePublic.name}`)

                        imagePublic.previewElement.classList.add('dz-sucess')
                        imagePublic.previewElement.classList.add('dz-complete')
                    }
                },
                success: function (file, response) {
                    document.querySelector('#imagen').value = response.success;
                    document.querySelector('#error-upload').innerHTML = " ";

                    file.nameserver = response.success

                },
                error: function (file, response) {
                    document.querySelector('#error-upload').innerHTML = '<div class="text-red-500 p-2 border-l-4 border-red-400 bg-red-100 mt-2"  role="alert">Formato no valido</div>';
                },
                maxfilesexceeded: function (file) {
                    if (this.files[1] != null) {
                        this.removeFile(this.files[0]);//Delete Archive
                        this.addFile(file);//Add New Archive
                    }
                },
                removedfile: function (file, response) {
                    file.previewElement.parentNode.removeChild(file.previewElement);
                    let params = {
                        imagen: file.nameserver ?? document.querySelector('#imagen').value,
                    }
                    axios.post('/vacantes/borrarImagen', params).then(resp => console.log(file))
                }

            })

        })
    </script>
@endsection
{{--End Section Yield Script--}}
