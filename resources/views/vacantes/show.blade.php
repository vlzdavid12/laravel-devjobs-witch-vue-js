@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css"
          integrity="sha512-Woz+DqWYJ51bpVk5Fv0yES/edIMXjj3Ynda+KWTIkGoynAMHrqTcDUQltbipuiaD5ymEo9520lyoVOo9jCQOCA=="
          crossorigin="anonymous"/>
@endsection

@section('content')
    <div class="container mx-auto">
        <h1 class="text-center text-gray-700 text-3xl mt-6 uppercase">{{$vacante->titulo}}</h1>
        <div class="mt-10 mb-20 md:flex items-start">
            <div class="md:w-3/5 p-5">

                <div class="mb-10">
                    <p class="block text-gray-700 font-bold my-2">
                        {{__('Publicado ')}}
                        <span class="font-normal">{{$vacante->created_at->diffForHumans()}}</span>
                    </p>
                    <p class="block text-gray-700 font-bold my-2">
                        {{__('Categoría: ')}}
                        <span class="font-normal">{{$vacante->categoria->nombre}}</span>
                    </p>
                    <p class="block text-gray-700 font-bold my-2">
                        {{__('Salario: ')}}
                        <span class="font-normal">{{$vacante->salario->nombre}}</span>
                    </p>
                    <p class="block text-gray-700 font-bold my-2">
                        {{__('Ubicación: ')}}
                        <span class="font-normal">{{$vacante->ubicacion->nombre}}</span>
                    </p>
                    <p class="block text-gray-700 font-bold my-2">
                        {{__('Experiencia: ')}}
                        <span class="font-normal">{{$vacante->experiencia->nombre}}</span>
                    </p>
                </div>

                <h3 class="text-gray-800 my-3 text-3xl text-left">{{__('Conocimientos y Tecnologías')}}</h3>
                @php
                    $listArray = explode(",",$vacante->habilidades);
                @endphp

                <ul class="flex flex-wrap">
                    @foreach($listArray as $item)
                        <li class="m-1 border-2 rounded border-gray-500 p-2 text-gray-500">{{$item}}</li>
                    @endforeach
                </ul>
                <a href="/storage/vacantes/{{$vacante->imagen}}" data-lightbox="{{$vacante->titulo}}"
                   data-title="{{$vacante->titulo}}">
                    <img src="/storage/vacantes/{{$vacante->imagen}}" alt="" class="w-28 my-5 block rounded"/>
                </a>
                <div class="mt-4 text-gray-700 leading-6">{!! $vacante->descripcion !!}</div>
                <h3 class="text-gray-800 my-5 text-3xl text-left">{{__('Datos Reclutador')}}</h3>

                <ul class="text-gray-700  leading-6">
                    <li>
                        <strong>{{__('Nombre: ')}}</strong>{{$vacante->reclutador->name }}
                    </li>
                    <li>
                        <strong>{{__('Email: ')}}</strong>{{$vacante->reclutador->email }}
                    </li>
                    <li>
                        <strong>{{__('Url: ')}}</strong>{{$vacante->reclutador->url }}
                    </li>
                </ul>
            </div>
            <div class="md:w-2/5">
                @include('ui.contacto')
            </div>
        </div>
    </div>
@endsection
