@extends ('layouts.app')
@section('navegacion')
    @include('ui.categoryNav')
@endsection
@section('content')
    <div class="flex lg:flex-row shadow bg-white mx-4">
        <div class="lg:w-1/2 px-8 lg:px-12 py-12 lg:py-24">
            <p class="text-5xl text-gray-700">
                dev<span class="font-bold">Jobs</span>
            </p>
            <h1 class="mt-2 sm:mt-4 text-3xl font-bold text-gray-700 leading-tight">
                Encuentra un trabajo remoto en tu país <span class="text-teal-500 block">
                    Para Desarrolladores & Diseñadores</span>
            </h1>
            @include('ui.search')
        </div>
        <div class="block lg:w-1/2">
            <img src="{{asset('images/4321.jpg')}}" class="p-4 object-cover h-full inset-0 object-right" alt="devjobs"/>
        </div>
    </div>
    <div class="lg:flex-row shadow bg-white mx-4 my-4 py-4">
        <h2 class="my-2 text-teal-600 pt-4 text-4xl text-center">Nuevas Vacantes</h2>
        <ul class="mt-5 lg:flex lg:flex-wrap justify-center">
            @foreach ($vacantes as $vacante)
                <li class="shadow bg-white m-2 p-4 border-gray-600 border-l-4 hover:bg-gray-100 lg:w-5/12">
                    <a href="{{route('vacantes.show', compact('vacante'))}}">
                        <h2 class="text-gray-600 py-2 text-2xl uppercase"> {{$vacante->titulo}}</h2>
                    </a>
                    <div class="flex flex-wrap">
                        <div class="text-teal-600 mr-2"><a href="{{route('category.show', ['categoria' => $vacante->categoria_id])}}" ><strong>{{__('Categoria: ')}}</strong>{{$vacante->categoria->nombre}}</a> |</div>
                        <div class="text-teal-600 mr-2"><strong>{{__('Experiencia: ')}}</strong>{{$vacante->experiencia->nombre}}</div>
                    </div>

                </li>
            @endforeach
        </ul>
    </div>
@endsection
