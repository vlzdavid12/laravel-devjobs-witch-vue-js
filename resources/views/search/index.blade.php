@extends ('layouts.app')
@section('content')
    <div class="container mx-auto p-4">
        <h2 class="my-2 text-gray-700 m-4 text-4xl uppercase text-center">Resultado</h2>
        <p class="text-center text-gray-700">Número: {{count($vacantes)}} </p>
        <ul class="mt-5 lg:flex lg:flex-wrap bg-white p-2 rounded">
            @if(count($vacantes) > 0)
                @foreach ($vacantes as $vacante)
                    <li class="shadow bg-white my-2 p-4 border-gray-600 border-l-4 hover:bg-gray-100 flex-1 m-1">
                        <a href="{{route('vacantes.show', compact('vacante'))}}">
                            <h2 class="text-gray-600 py-2 text-2xl uppercase"> {{$vacante->titulo}}</h2>
                            <div class="flex flex-wrap text-1xl">
                                <div class="text-teal-600 mr-2"><strong>{{__('Categoria: ')}}</strong>{{$vacante->categoria->nombre}} |</div>
                                <div class="text-teal-600 mr-2"><strong>{{__('Experiencia: ')}}</strong>{{$vacante->experiencia->nombre}}</div>
                            </div>
                        </a>
                    </li>
                @endforeach
            @else
                <div class="text-center p-4 mx-auto text-blue-500">{{__('No hay resultados en la busqueda')}}</div>
            @endif
        </ul>
    </div>
@endsection
