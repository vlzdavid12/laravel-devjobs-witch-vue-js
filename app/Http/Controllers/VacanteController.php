<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Experiencia;
use App\Models\Salario;
use App\Models\Ubicacion;
use App\Models\Vacante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class VacanteController extends Controller
{

    public function __construct()
    {

        //$this->middleware(['auth', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //auth()->user()->vacantes;
        $vacantes = Vacante::where('user_id', auth()->user()->id)->simplePaginate(12);

        //Method Compact
        return view('vacantes.index', compact('vacantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Categoria::all();
        $experiencia = Experiencia::all();
        $ubicacion = Ubicacion::all();
        $salarios = Salario::all();

        //Method With
        return view('vacantes.create')
            ->with('categorias', $category)
            ->with('experiencias', $experiencia)
            ->with('ubicaciones', $ubicacion)
            ->with('salarios', $salarios);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validations
        $data = $request->validate([
            "titulo" => 'required|min:8',
            "categoria" => 'required',
            "experiencia" => 'required',
            "ubicacion" => 'required',
            "salario" => 'required',
            "descripcion" => 'required|min:20',
            "habilidades" => 'required',
            "imagen" => 'required',
        ]);

        //Insert DB
        auth()->user()->vacantes()->create([
            'titulo' => $data['titulo'],
            'descripcion' => $data['descripcion'],
            'imagen' => $data['imagen'],
            'habilidades' => $data['habilidades'],
            'categoria_id' => $data['categoria'],
            'experiencia_id' => $data['experiencia'],
            'ubicacion_id' => $data['ubicacion'],
            'salario_id' => $data['salario'],
        ]);

        return redirect()->route('vacantes.index');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Vacante $vacante
     * @return \Illuminate\Http\Response
     */
    public function show(Vacante $vacante)
    {
        return view('vacantes.show')->with('vacante', $vacante);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Vacante $vacante
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacante $vacante)
    {
        //Add Policy
        $this->authorize('update', $vacante);


        //Consult Select Form
        $category = Categoria::all();
        $experiencia = Experiencia::all();
        $ubicacion = Ubicacion::all();
        $salarios = Salario::all();

        //Method With
        return view('vacantes.edit')
            ->with('categorias', $category)
            ->with('experiencias', $experiencia)
            ->with('ubicaciones', $ubicacion)
            ->with('salarios', $salarios)
            ->with('vacante', $vacante);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Vacante $vacante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vacante $vacante)
    {
        //Add Policy
        $this->authorize('update', $vacante);

        //Validations
        $data = $request->validate([
            "titulo" => 'required|min:8',
            "categoria" => 'required',
            "experiencia" => 'required',
            "ubicacion" => 'required',
            "salario" => 'required',
            "descripcion" => 'required|min:20',
            "habilidades" => 'required',
            "imagen" => 'required',
        ]);

        $vacante->titulo = $data['titulo'];
        $vacante->descripcion = $data['descripcion'];
        $vacante->imagen = $data['imagen'];
        $vacante->habilidades = $data['habilidades'];
        $vacante->categoria_id = $data['categoria'];
        $vacante->experiencia_id = $data['experiencia'];
        $vacante->ubicacion_id = $data['ubicacion'];
        $vacante->salario_id = $data['salario'];

        $vacante->save();


        return redirect()->action([VacanteController::class, 'index']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Vacante $vacante
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacante $vacante)
    {
        //Add Policy
        $this->authorize('delete', $vacante);

        $vacante->delete();

        return response()->json(['msg' => "Se ha eliminado esta vacante " . $vacante->titulo]);
    }

    /**
     * Upload Image Vacante
     **/

    public function imagen(Request $request)
    {
        //$request->all();

        $imagen = $request->file('file');
        $nombreImagen = time() . '.' . $imagen->extension();
        $imagen->move(public_path('storage/vacantes'), $nombreImagen);
        return response()->json(['success' => $nombreImagen]);
    }

    /**
     * Erase Image via axios Ajax
     **/
    public function borrarImagen(Request $request)
    {
        if ($request->ajax()) {
            $imagen = $request->get('imagen');
            if (FILE::exists('storage/vacantes/' . $imagen)) {
                FILE::delete('storage/vacantes/' . $imagen);
            }
        }
        return response('Delete Imagen', 200);
    }

    /**
     *Update Vacante
     **/
    public function updateState(Request $request, $id)
    {

        //Read New Search State Modify
        $vacante = Vacante::find($id);
        $vacante->active = $request->state;

        //Save DB
        $vacante->save();

        return response()->json(["success" => "Success Fully"]);
    }

    public function search(Request $request)
    {
        //dd($request->all());
        //Validation
        $data = $request->validate([
            'categoria' => 'required',
            'ubicacion' => 'required'
        ]);

        $category = $data['categoria'];
        $ubication = $data['ubicacion'];

        /*Method And*/
         $vacantes = Vacante::latest()
            ->where('categoria_id', $category)
            ->where('ubicacion_id', $ubication)
            ->get();

        /*Method Or
       $vacantes = Vacante::latest()
         ->where('categoria_id', $category)
         ->orWhere('ubicacion_id', $ubication)
         ->get();*/

        /*$vacantes = Vacante::where([
            'categoria_id'=>$category,
            'ubicacion_id'=>$ubication
        ])->get();*/

        return view('search.index', compact('vacantes'));
    }

    public function results()
    {
        return "Mosatrar Resultados";
    }


}
