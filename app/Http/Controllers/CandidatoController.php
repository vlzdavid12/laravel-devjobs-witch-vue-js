<?php

namespace App\Http\Controllers;

use App\Models\Candidato;
use App\Models\Vacante;
use App\Notifications\NuevoCandidato;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;

class CandidatoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {

        $id = $request->route('id');
        $vacante = Vacante::findOrFail($id);

        $this->authorize('view', $vacante);

        $candidatos = $vacante->candidatos;

        return view('candidatos.index', compact('candidatos'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->validate([
            "nombre" => "required|min:8",
            "email" => "required|min:10|email",
            "cv" => "required|mimes:pdf|max:1000",
            "vacante_id" => "required"
        ]);


        //Consult Register Vacante in Relations
        $vacante = Vacante::find($data['vacante_id']);

        //Upload Archive pdf
        if($request->file('cv'))
        {
            $archivo = $request->file('cv');
            $nombreArchivo = time() . "." . $request->file('cv')->extension();
            $ubicacion = public_path('/storage/cv');
            $archivo->move($ubicacion, $nombreArchivo);
        }


        /**Insert DB No.3**/

        $vacante->candidatos()->create([
           'nombre' => $data['nombre'],
           'email' => $data['email'],
            'cv' => '123456.pdf'
        ]);

        /**Send Email New Vacante**/
        //Relation Models Vacante
        $reclutador =  $vacante->reclutador;
        $reclutador->notify(new NuevoCandidato($vacante->titulo, $vacante->id));

        return back()->with('state', 'Los datos se han enviado correctamente');

        /**Insert DB No.1
        $candidato = new Candidato();
        $candidato->nombre = $data['nombre'];
        $candidato->email = $data['email'];
        $candidato->vacante_id = $data['vacante_id'];
        $candidato->cv = $nombreArchivo;

        $candidato->save();**/

        /**Insert DB No.2
        $candidato = new Candidato($data);
        $candidato->cv = '345.pdf';
        $candidato->save();**/

        /**Insert No. 3
        $candidato = new Candidato();
        $candidato->fill($data);
        $candidato->cv = '345.pdf';
        $candidato->save();**/



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Candidato  $candidato
     * @return \Illuminate\Http\Response
     */
    public function show(Candidato $candidato)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Candidato  $candidato
     * @return \Illuminate\Http\Response
     */
    public function edit(Candidato $candidato)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Candidato  $candidato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidato $candidato)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Candidato  $candidato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidato $candidato)
    {
        //
    }
}
