<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacante extends Model
{
    protected $fillable = [
        'titulo',
        'descripcion',
        'imagen',
        'habilidades',
        'categoria_id',
        'experiencia_id',
        'ubicacion_id',
        'salario_id'
    ];

    //Create Relations 1:1 categoria y vacante
    public function categoria(){
        return $this->belongsTo(Categoria::class);
    }

    //Create Relations 1:1 salario y vacante
    public function salario(){
        return $this->belongsTo(Salario::class);
    }

    //Create Relations 1:1 ubicacion y vacante
    public function ubicacion(){
        return $this->belongsTo(Ubicacion::class);
    }

    //Create Relations 1:1 experiencia y vancate
    public function experiencia(){
        return $this->belongsTo(Experiencia::class);
    }

    //Create Relations 1:1 reclutador y vacante
    public function reclutador(){
        return $this->belongsTo(User::class, 'user_id');
    }

    //Create Relations 1:n vacante y candidatos
    public function candidatos(){
        return $this->hasMany(Candidato::class);
    }

}
